import {
  findSchemasByTitleAndMajor,
  findSchemaPaths,
  readConfig,
  readObject,
  schemaVersion,
} from "@wikimedia/jsonschema-tools";
import Ajv from "ajv";
import draft07 from "ajv/lib/refs/json-schema-draft-07.json";
import { describe, expect, test } from "vitest";
import path from "path";

const ajv = new Ajv({
  schemaId: "$id",
});

draft07.$id = "https://json-schema.org/draft-07/schema#";
draft07.$schema = "https://json-schema.org/draft-07/schema#";
ajv.addMetaSchema(draft07, "https://json-schema.org/draft-07/schema");

interface Value {
  $schema: string;
  [key: string]: any;
}

interface Schema {
  title: string;
  $id: string;
  $schema: string;
  type: string;
  properties: object;
  [key: string]: any;
}

interface findSchemasByTitleAndMajorReturnValue {
  [title: string]: {
    [major_version: string]: {
      title: string;
      path: string;
      version: string;
      current: boolean;
      contentType: string;
      schema: Schema;
    }[];
  };
}

const validateConfig = (configSchema: Schema, config: Value) => {
  // AJV caches schemas so remove it
  ajv.removeSchema(configSchema);
  expect(ajv.validate(configSchema, config));
};

const valuePathToInfo = async (valuePath: string) => {
  const values: Value = await readObject(valuePath);
  const parsedPath = path.parse(valuePath);
  return {
    path: valuePath,
    version: schemaVersion(values, "$schema") as string,
    contentType: parsedPath.ext.slice(1),
    values,
  };
};

type ValueInfo = Awaited<ReturnType<typeof valuePathToInfo>>;

const addSchemaInfoToValueInfo = (
  valueInfo: ValueInfo
): ValueInfo & {
  schema_id: string;
  schema_title: string;
  major_version: string;
} => {
  // Extract the path from the `$schema` and use the `version` field
  const schemaPathParts = valueInfo.values["$schema"].split("/");
  // Remove the last part (version) and join back to get the path
  schemaPathParts.pop(); // Remove version assuming it's the last part
  const title = schemaPathParts.join("/").substring(1); // Remove the leading '/'
  const schema_id = `${title}/${valueInfo.version}`;
  const major_version = valueInfo.version.split(".")[0];

  return {
    ...valueInfo,
    schema_id,
    schema_title: title,
    major_version,
  };
};

describe("Config confirmity", async () => {
  const valuePaths = findSchemaPaths({
    schemaBasePath: "./values/",
    contentTypes: ["yaml"],
    currentName: "values",
  }) as string[];

  const options = readConfig({}, true);
  const allSchemas = findSchemasByTitleAndMajor(
    options
  ) as findSchemasByTitleAndMajorReturnValue;

  const valueInfos = await Promise.allSettled(
    valuePaths.map((path: string) => valuePathToInfo(path))
  ).then((results: PromiseSettledResult<ValueInfo>[]) =>
    results
      .filter(
        (result: PromiseSettledResult<ValueInfo>) =>
          result.status === "fulfilled"
      )
      .map((result: PromiseFulfilledResult<ValueInfo>) =>
        addSchemaInfoToValueInfo(result.value)
      )
  );

  const valuesSchemaMap = valueInfos.map((valueInfo) => ({
    schema: allSchemas[valueInfo.schema_title][valueInfo.major_version].find(
      ({ version }) => valueInfo.version === version
    ).schema,
    values: valueInfo.values,
    value_path: valueInfo.path,
  }));

  valuesSchemaMap.forEach(({ schema, values, value_path }) => {
    test(`${value_path} validates against ${schema.$id}`, () => {
      validateConfig(schema, values);
    });
  });
});
